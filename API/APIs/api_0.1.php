<?php

	/**
	 * API
	 */
	class API
	{
		private $v = '0.1';
		
		private $last_resp;

		private $resp = [];

		function __construct($args = [])
		{
			$this->db = $GLOBALS['db'];
		}

		public function __call( $method, $params ) {
			$object = substr($method, 0, strpos($method, '.'));
			if(!$object) $method();
			$method_name = substr($method, strpos($method, '.')+1);
			include_once __DIR__.'/../source/'.$object.'.methods.php';
			$resp = $object::$method_name($params);
			if(!empty($resp))
				$this->last_resp = $resp;
			else
				$this->last_resp = null;
		}

		function add_resp($req_name){
			if($this->last_resp === null) return false;
			$req = $this->last_resp;
			$this->resp[$req_name] = $req;
		}

		function response(){
			exit ( json_encode($this->resp) );
		}

	}

?>