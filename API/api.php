<?php
	header('Content-Type: application/json'); // Устанавливаем тип данных на json

	$reqs = $_POST['reqs'];
	$reqs = @json_decode($reqs, true); // Конвертируем json в php массив (ассоциативный)

	$api_v = $_POST['api_v']; // 
	$path = __DIR__.'/APIs/api_'.$api_v.'.php'; // Составляем путь к файлу и после проверяем, что он существует
	if(!file_exists($path))
		exit; // Здесь было бы правильнее вывести ошибку
	include_once __DIR__.'/APIs/api_'.$api_v.'.php';
	// Инициализируем API
	$api = new API();

	// Проходимся по каждому запросу, вызываем определенный метод и сохраняем его результат в массив API
	foreach ($reqs as $req) {
		$method = $req['method'];
		$params = $req['params'];
		$req_name = $req['name'];
		$api->$method($params);
		$api->add_resp($req_name);
	}
	// Возвращаем все результаты
	$api->response();